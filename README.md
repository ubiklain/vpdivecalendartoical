# Documentation

Ce projet permet de récupérer les calendriers public du site de plongée sous-marine VPDIVE.
Il les publie ensuite sur un site WEB qui permet de souscrire à un ICAL depuis votre client de calendrier

## Crontab :

Installer ces commandes crontab sur le user ayant les bons droits :

```bash
    ____________________________________________ Minutes (à chaque début d'heure)
#   |        ____________________________________ Hours (à 8h, 13h et 19h)
#   |       |        ____________________________ Days of Month (tous les jours)
#   |       |       |        ____________________ Months (tous les mois)
#   |       |       |       |        ____________ Days of Week (tous les jours de la semaine)
#   |       |       |       |       |         ___ Command to execute (`which php`si besoin de trouver où se trouve votre php + `remplacer le dossier par celui de votre projet`)
#___|_______|_______|_______|_______|________|_________________________________

0 8,13,19 * * * /usr/bin/php /var/www/plongee/jsonparsercodep.php
```
