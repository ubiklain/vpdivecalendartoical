<?php

// Variables de dates à récupérer
$firstDayOfYear = mktime(0, 0, 0, 1, 1, date("Y"));
$firstDayOfYear = date("Y-m-d", $firstDayOfYear);

$lastDayOfNextYear = mktime(0, 0, 0, 12, 31, date("Y")+1);
$lastDayOfNextYear = date("Y-m-d", $lastDayOfNextYear);


function query($url, $outfile){

    $myfile = fopen($outfile, "w");

    //init curl
    $ch = curl_init($url);

    // Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    // Return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Execute the POST request
    $result = curl_exec($ch);

    // Close cURL resource
    curl_close($ch);


    //var_dump(json_decode($result));
    $jsonres = json_decode($result);
    echo count($jsonres)." résultats pour ".$outfile.".\r\n";

    if($jsonres !==null && count($jsonres)>0){

        $txt="BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nBEGIN:VTIMEZONE\nTZID:Europe/Paris\nBEGIN:DAYLIGHT\nTZOFFSETFROM:+0100\nRRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\nDTSTART:19810329T020000\nTZNAME:UTC+2\nTZOFFSETTO:+0200\nEND:DAYLIGHT\nBEGIN:STANDARD\nTZOFFSETFROM:+0200\nRRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\nDTSTART:19961027T030000\nTZNAME:UTC+1\nTZOFFSETTO:+0100\nEND:STANDARD\nEND:VTIMEZONE\n";
        fwrite($myfile, $txt);

        foreach($jsonres as $j){

        $txt = "BEGIN:VEVENT\n";
            fwrite($myfile, $txt);

        $txt = "SUMMARY:".$j->{'title'}."\n";
        fwrite($myfile, $txt);

        $start = substr($j->{'start'},0,4).substr($j->{'start'},5,2).substr($j->{'start'},8,3).substr($j->{'start'},11,2).substr($j->{'start'},14,2);
        $txt = "DTSTART;TZID=Europe/Paris:".$start."00\n";
            fwrite($myfile, $txt);

        $end = substr($j->{'end'},0,4).substr($j->{'end'},5,2).substr($j->{'end'},8,5).substr($j->{'end'},14,2);
        $txt = "DTEND;TZID=Europe/Paris:".$end."00\n";
            fwrite($myfile, $txt);

        $txt = "UID:".$j->{'id'}."\n";
            fwrite($myfile, $txt);

        $txt = "DESCRIPTION:\n";
            fwrite($myfile, $txt);

        $txt = "END:VEVENT\n";
            fwrite($myfile, $txt);

        }

    $txt = "END:VCALENDAR\n";
    fwrite($myfile, $txt);
}


}

// ical
$url = 'https://subaquagone-plongee.fr/f/calendar/events/u70KPhil9QpjCmfTDWLIwMVBAaOUt82zEGFqX4sHd5o?ms=&start='.$firstDayOfYear.'&end='.$lastDayOfNextYear.'&_=1697193736493';
$outfile = "/var/www/plongee/subaquagone.ical";
query($url, $outfile);

