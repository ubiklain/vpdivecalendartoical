<?php

// Variables de dates à récupérer
$firstDayOfYear = mktime(0, 0, 0, 1, 1, date("Y"));
$firstDayOfYear = date("Y-m-d", $firstDayOfYear);

$lastDayOfNextYear = mktime(0, 0, 0, 12, 31, date("Y")+1);
$lastDayOfNextYear = date("Y-m-d", $lastDayOfNextYear);


function query($url, $outfile){

    $myfile = fopen($outfile, "w");

    //init curl
    $ch = curl_init($url);

    // Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    // Return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Execute the POST request
    $result = curl_exec($ch);

    // Close cURL resource
    curl_close($ch);


    $jsonres = json_decode($result);


    echo count($jsonres)." résultats pour ".$outfile.".\r\n";

    if($jsonres !==null && count($jsonres)>0){

        $txt="BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nBEGIN:VTIMEZONE\nTZID:Europe/Paris\nBEGIN:DAYLIGHT\nTZOFFSETFROM:+0100\nRRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\nDTSTART:19810329T020000\nTZNAME:UTC+2\nTZOFFSETTO:+0200\nEND:DAYLIGHT\nBEGIN:STANDARD\nTZOFFSETFROM:+0200\nRRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\nDTSTART:19961027T030000\nTZNAME:UTC+1\nTZOFFSETTO:+0100\nEND:STANDARD\nEND:VTIMEZONE\n";
        fwrite($myfile, $txt);

        foreach($jsonres as $j){

        $txt = "BEGIN:VEVENT\n";
            fwrite($myfile, $txt);

        $txt = "SUMMARY:".$j->{'title'}."\n";
        fwrite($myfile, $txt);

        $start = substr($j->{'start'},0,4).substr($j->{'start'},5,2).substr($j->{'start'},8,3).substr($j->{'start'},11,2).substr($j->{'start'},14,2);
        $txt = "DTSTART;TZID=Europe/Paris:".$start."00\n";
            fwrite($myfile, $txt);

        $end = substr($j->{'end'},0,4).substr($j->{'end'},5,2).substr($j->{'end'},8,5).substr($j->{'end'},14,2);
        $txt = "DTEND;TZID=Europe/Paris:".$end."00\n";
            fwrite($myfile, $txt);

        $txt = "UID:".$j->{'id'}."\n";
            fwrite($myfile, $txt);

        $txt = "DESCRIPTION:\n";
            fwrite($myfile, $txt);

        $txt = "END:VEVENT\n";
            fwrite($myfile, $txt);

        }

    $txt = "END:VCALENDAR\n";
    fwrite($myfile, $txt);
}

}

// ical CODEP
$url = 'https://technique.codep69-ffessm.fr/f/calendar/events/zAPgYjf65e72wZIKRoExBr1DuOkcLVGy9CMNmTtl4WF?ms=&start='.$firstDayOfYear.'&end='.$lastDayOfNextYear.'&_=1696970335503';
$outfile = "/var/www/plongee/codep69.ical";
query($url, $outfile);


// ical CODEP GP
$url = 'https://technique.codep69-ffessm.fr/f/calendar/events/ULtEB4PQpeyDNWf09dmlrw1xXCnjbozYHSik7IFgc8M?ms=&start='.$firstDayOfYear.'&end='.$lastDayOfNextYear.'&_=1696971152514';
$outfile = "/var/www/plongee/codep69gp.ical";
query($url, $outfile);

// ical CODEP MF1
$url = 'https://technique.codep69-ffessm.fr/f/calendar/events/WLbYgkPRwxNcMmhBFn4rof9CvsUGledIK8S2T3HXAEz?ms=&start='.$firstDayOfYear.'&end='.$lastDayOfNextYear.'&_=1696971152514';
$outfile = "/var/www/plongee/codep69mf1.ical";
query($url, $outfile);


// ical CODEP BIO
$url = 'https://bio.codep69-ffessm.fr/f/calendar/events/p8HgkA21O7UDYRTCc4bx9GNSBzVjWuKvqdeh60IPLXs?ms=&start='.$firstDayOfYear.'&end='.$firstDayOfYear.'&_=1697041123437';
$outfile = "/var/www/plongee/codep69bio.ical";
query($url, $outfile);

// ical CODEP PHOTO
$url = 'https://photo.codep69-ffessm.fr/f/calendar/events/3B1qkxKCWEgZbJUiF4LMOaINXnr7yvlfsSzAVPRjpQ8?ms=&start='.$firstDayOfYear.'&end='.$firstDayOfYear.'&_=1735414669247';
$outfile = "/var/www/plongee/codep69photo.ical";
query($url, $outfile);
